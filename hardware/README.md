#Hardware

_BOM.xlsx_ includes the build-of-materials for this project including Farnell links. If you are sourcing the components from another website, please make sure the resistors are 1% thin-film or better. The analog circuitry has very fine tolerances.

